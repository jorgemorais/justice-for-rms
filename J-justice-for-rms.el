;; -*- lexical-binding: t; -*-
; SPDX-FileCopyrightText: 2021 Jorge P. de Morais Neto <jorge+git@disroot.org>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(require 'ox-publish)
;; The string on the below line must be the path of the repository; edit if not
(let ((repo-path "~/repos/justice-for-rms/"))
  (add-to-list 'org-publish-project-alist
	       `("justice-for-rms"
		 :base-directory ,(concat repo-path "publish/")
		 :publishing-directory ,(concat repo-path "public/")
		 :publishing-function org-html-publish-to-html)))

(provide 'J-justice-for-rms)
;;; J-justice-for-rms.el ends here
