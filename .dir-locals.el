;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((nil . ((fill-column . 80)
	 (ispell-local-dictionary . "en_US")))
 (org-mode . ((org-footnote-section . nil)))
 ("public/post"    . ((nil . ((fill-column . 998)))))
 ("public/post/es" . ((nil . ((ispell-local-dictionary . "es_ES"))))))
 ("public/post/pt" . ((nil . ((ispell-local-dictionary . "pt_BR"))))))
