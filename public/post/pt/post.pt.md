# Justiça para Dr. Richard Matthew Stallman

#tolerancia #rms #softwarelivre #gnu #liberdade #reinstallrms

![Dr. Richard Matthew Stallman](http://stallmansupport.org/images/richard_stallman.png)

Richard M. Stallman, conhecido como _rms_ na comunidade software livre, é um físico, cientista da computação, filósofo, e ativista apaixonado pela liberdade de software.  Ele é fundador e líder do movimento software livre[^1] (muitas vezes chamado erroneamente[^2] de "código aberto").  O movimento software livre inspirou ainda projetos como Creative Commons e Wikipédia.

Em 1983, rms fundou[^3] o projeto GNU[^4] visando construir um sistema operacional livre (conhecido hoje como _GNU/Linux_).  Em 1985 ele fundou a _Free Software Foundation_ (FSF, Fundação para o Software Livre), para levantar fundos e prover infraestrutura para o desenvolvimento do GNU.  Ele desenvolveu o _GNU Compiler Collection_ (GCC)[^5], o GNU Debugger (GDB)[^6] e o GNU Emacs.[^7]  Ele inventou o conceito de copyleft[^8] e, baseado nele, escreveu a Licença Pública Geral GNU (GPL).[^9]

Em setembro de 2019, surgiu uma campanha difamatória pela Internet---baseada em informações falsas---que levou a uma série de falsas acusações, erros de interpretação, intolerância e desproporcionalidade.  Isso forçou rms a renunciar de sua posição no Instituto de Tecnologia de Massachusetts (MIT, _Massachusetts Institute of Technology_) e até da FSF que ele próprio fundou e liderava.   O anúncio de seu retorno ao conselho de administração da FSF em março de 2021 reacendeu a campanha.

Mais em: [In Support of Richard Stallman](https://stallmansupport.org/) (língua inglesa).

Assine a carta aberta em apoio a rms: [An open letter in support of Richard M. Stallman](https://rms-support-letter.github.io/index-pt-br.html) (traduzida para português).

Veja também [Justiça para Dr. Richard Matthew Stallman](https://jorgemorais.gitlab.io/justice-for-rms/index.pt.html) (melhorado; em português).

Por favor compartilhe em muitas redes sociais e divulgue!

[^1]: https://www.gnu.org/philosophy/free-sw.html "O que é o software livre? - Projeto GNU - Free Software Foundation"

[^2]: https://www.gnu.org/philosophy/open-source-misses-the-point.html "Por que o Código Aberto não compartilha dos objetivos do Software Livre - Projeto GNU - Free Software Foundation"

[^3]: https://www.gnu.org/gnu/initial-announcement.html "Anúncio Inicial - Projeto GNU - Free Software Foundation"

[^4]: https://www.gnu.org "O Sistema Operacional GNU e o Movimento de Software Livre"

[^5]: https://gcc.gnu.org/ "GCC, the GNU Compiler Collection - GNU Project - Free Software Foundation (FSF)"

[^6]: https://www.gnu.org/software/gdb "GDB: The GNU Project Debugger"

[^7]: https://www.gnu.org/software/emacs/ "GNU Emacs - GNU Project"

[^8]: https://www.gnu.org/licenses/copyleft.html "O Que é Copyleft? - Projeto GNU - Free Software Foundation"

[^9]: https://www.gnu.org/licenses/gpl-3.0.html "A Licença Pública Geral GNU v3.0 - Projeto GNU - Free Software Foundation"
