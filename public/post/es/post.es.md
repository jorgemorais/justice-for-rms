# Justicia para el Dr. Richard Matthew Stallman

#tolerancia #rms #softwarelibre #gnu #libertad

![Dr. Richard Matthew Stallman](http://stallmansupport.org/images/richard_stallman.png)

Richard M. Stallman, conocido como _rms_ en la comunidad del software libre, es un físico, informático, filósofo y activista apasionado por la libertad del software.  Es el fundador y líder del movimiento del software libre[^1] (a menudo mal llamado[^2] código abierto).  El movimiento del software libre también inspiró proyectos como Creative Commons y Wikipedia.

En 1983, rms fundó[^3] el proyecto GNU[^4] con el objetivo de construir un sistema operativo libre (conocido hoy como _GNU/Linux_).  En 1985 fundó la _Free Software Foundation_ (FSF, Fundación por el Software Libre), para recaudar fondos y proporcionar infraestructura para el desarrollo de GNU.  Desarrolló _GNU Compiler Collection_ (GCC)[^5], GNU Debugger (GDB)[^6] y GNU Emacs[^7].  Creó el concepto de copyleft[^8] y, basándose en él, escribió la Licencia Pública General GNU (GPL)[^9].

En septiembre de 2019 surgió en Internet una campaña de difamación, basada en información falsa, que generó una serie de acusaciones falsas, malas interpretaciones, intolerancia y respuestas desproporcionadas.  Esto obligó rms a renunciar a su puesto en el Instituto de Tecnología de Massachusetts (MIT, _Massachusetts Institute of Technology_) e incluso en la FSF que fundó y dirigió.  El anuncio de su regreso a la junta directiva de la FSF en marzo de 2021 reavivó la campaña.

Más en: [In Support of Richard Stallman](https://stallmansupport.org/) (en ingles).

Firmar carta abierta en apoyo de rms: [Una carta abierta en apoyo de Richard M. Stallman](https://rms-support-letter.github.io/index-es.html).

Vea también [Justicia para el Dr. Richard Matthew Stallman](https://jorgemorais.gitlab.io/justice-for-rms/index.es.html) (mejorado y traducido al español).

¡Comparta en muchas redes sociales y corra la voz!

[^1]: https://www.gnu.org/philosophy/free-sw.html "¿Qué es el software libre? - Proyecto GNU - Free Software Foundation"

[^2]: https://www.gnu.org/philosophy/open-source-misses-the-point.html "Por qué el «código abierto» pierde de vista lo esencial del software libre - Proyecto GNU - Free Software Foundation"

[^3]: https://www.gnu.org/gnu/initial-announcement.html "Anuncio inicial - Proyecto GNU - Free Software Foundation"

[^4]: https://www.gnu.org "El sistema operativo GNU y el movimiento del software libre"

[^5]: https://gcc.gnu.org/ "GCC, the GNU Compiler Collection - GNU Project - Free Software Foundation (FSF)"

[^6]: https://www.gnu.org/software/gdb "GDB: The GNU Project Debugger"

[^7]: https://www.gnu.org/software/emacs/ "GNU Emacs - GNU Project"

[^8]: https://www.gnu.org/licenses/copyleft.html "¿Qué es el copyleft? - Proyecto GNU - Free Software Foundation"

[^9]: https://www.gnu.org/licenses/gpl-3.0.html "A Licença Pública Geral GNU v3.0 - Projeto GNU - Free Software Foundation"
