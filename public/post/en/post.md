# Justice for Dr. Richard Matthew Stallman

#tolerance #rms #freesoftware #gnu #healingcommunities #restoretruth #reinstallrms #freethought

![Dr. Richard Matthew Stallman](http://stallmansupport.org/images/richard_stallman.png)

Richard M. Stallman, also known as _rms_ in the free software community, is a physicist, computer scientist, philosopher and passionate champion for software freedom.  He is the founder and leader of the Free Software Movement[^1] (often mistakenly[^2] called “open source”).  Beyond computing, he inspired projects like Creative Commons and Wikipedia.

In 1983, he launched[^3] the GNU Project[^4] with the goal of building a free software operating system (known today as _GNU/Linux_).  In 1985 Stallman founded the Free Software Foundation to raise funds and provide infrastructure for the development of GNU.  He developed the GNU Compiler Collection (GCC)[^5], the GNU Debugger (GDB)[^6] and GNU Emacs.[^7] He invented the concept of copyleft[^8] and, based on that, he wrote the GNU General Public License.[^9]

An Internet defamatory campaign broke up in September 2019, based on misinformation that led to a series of false accusations, mischaracterizations, intolerance and disproportionality.  This forced rms to resign from his position at MIT (Massachusetts Institute of Technology) and even from the FSF that he founded and led.  The announcement of his return to the FSF Board of Directors in March 2021 reignited the slanderous campaign.

More at: [In Support of Richard Stallman](https://stallmansupport.org/).

Sign the open letter in rms support: [An open letter in support of Richard M. Stallman](https://rms-support-letter.github.io/)

Please share in many social networks and publicize!

[^1]: https://www.gnu.org/philosophy/free-sw.html "What is free software? - GNU Project - Free Software Foundation"

[^2]: https://www.gnu.org/philosophy/open-source-misses-the-point.html "Why Open Source Misses the Point of Free Software - GNU Project - Free Software Foundation"

[^3]: https://www.gnu.org/gnu/initial-announcement.html "Initial Announcement - GNU Project - Free Software Foundation"

[^4]: https://www.gnu.org/ "The GNU Operating System and the Free Software Movement"

[^5]: https://gcc.gnu.org/ "GCC, the GNU Compiler Collection - GNU Project - Free Software Foundation (FSF)"

[^6]: https://www.gnu.org/software/gdb "GDB: The GNU Project Debugger"

[^7]: https://www.gnu.org/software/emacs/ "GNU Emacs - GNU Project"

[^8]: https://www.gnu.org/licenses/copyleft.html "What is Copyleft?"

[^9]: https://www.gnu.org/licenses/gpl-3.0.html "The GNU General Public License v3.0 - GNU Project - Free Software Foundation"
